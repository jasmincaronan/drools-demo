package com.cloudsherpas.droolsample.data.repository;

import com.cloudsherpas.droolsample.data.domain.RuleArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author RMPader
 */
public interface RuleArtifactRepository extends PagingAndSortingRepository<RuleArtifact, Long> {

    RuleArtifact findByActiveTrue();

}
