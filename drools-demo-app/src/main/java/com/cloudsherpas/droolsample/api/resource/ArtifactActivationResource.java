package com.cloudsherpas.droolsample.api.resource;

/**
 * @author RMPader
 */
public class ArtifactActivationResource {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
